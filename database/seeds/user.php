<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class user extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run ()
  {

    \App\Role::create([
      'alias' => 'admin'
    ]);

    \App\Role::create([
      'alias' => 'user'
    ]);


    \App\User::create([
      'name' => 'test',
      'email' => 'test@gmail.com',
      'role_id' => 2,
      'password' => Hash::make('testest')
    ]);
  }
}
