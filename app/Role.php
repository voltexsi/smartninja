<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{


  protected $primaryKey = 'role_id';


  protected $table = 'role';



  protected $fillable = [
    'alias'
  ];

  /**
   * @return \App\User
   */
  public function user ()
  {
    return $this->hasMany(User::class, 'role_id', 'role_id');
  }
}
