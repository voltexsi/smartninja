<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddService;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ServiceController extends Controller
{
  public function addService()
  {
    return View::make('service.add');
  }
  
  
  public function saveService(AddService $request)
  {
    $name = $request->get('service_name');
    $shortDesc = $request->get('short_description');
    $desc = $request->get('description');
    $userID = Auth::user()->user_id;
    $alias = str_slug($name);
    
    $newService = Service::create([
      'service_name' => $name,
      'service_alias' => $alias,
      'user_id' => $userID,
      'short_description' => $shortDesc,
      'description' => $desc,
    ]);
    
    
    return back();
  }
  
  public function listService()
  {
    $services = Service::all();
    
    
    return View::make('service.list', ["services" => $services]);
  }
  
  public function editService($id)
  {
    $service = Service::find($id);
    
    return View::make('service.edit', ["service" => $service]);
  }
  
  public function updateService($id, Request $request)
  {
    $service = Service::find($id);
    
    
    $name = $request->get('service_name');
    $shortDesc = $request->get('short_description');
    $desc = $request->get('description');
    $alias = str_slug($name);
    
    
    $service->service_name = $name;
    $service->short_description = $shortDesc;
    $service->description = $desc;
    $service->service_alias = $alias;
    
    $service->save();
    
    return back()->with("success", "Data saved");
    
  }
  
}
