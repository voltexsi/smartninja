<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
  public function listUsers ()
  {

    //users

    $users = User::all();
    return View::make('user.list', ['users' => $users]);
  }

  public function search(){
    return View::make('user.search', []);
  }
}
