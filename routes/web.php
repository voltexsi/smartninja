<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

Route::get('/about-us', 'BlogController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');


// /admin

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'acl']], function () {
  Route::get('service/add', 'ServiceController@addService');
  Route::post('service/save', 'ServiceController@saveService');
  Route::get('service/list', 'ServiceController@listService');
  Route::get('service/edit/{id}', 'ServiceController@editService');
  Route::post('service/update/{id}', 'ServiceController@updateService');
});

Route::group(['prefix' => 'ratings', 'middleware' => ['web', 'auth', 'acl']], function () {
  Route::post('users', function () {
    echo 'dela';
    exit;
  });
});
// /