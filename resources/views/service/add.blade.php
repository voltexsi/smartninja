@extends('layouts.app')


@section('content')
    <div class="container">
        {!! Form::open(['method' => 'post', 'url' => 'admin/service/save']) !!}
        <h2>Add service</h2>

        @if(count($errors->all()) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $message)
                    {!! $message !!} <br>
                @endforeach
            </div>
        @endif
        <div class="form-group">
            <label for="name">Service name</label>
            <input id="name" type="text" required class="form-control" name="service_name"/>
        </div>

        <div class="form-group">
            <label for="short-desc">Short description</label>
            <input id="short-desc" type="text" class="form-control" name="short_description"/>
        </div>

        <div class="form-group">
            <label for="desc">Description</label>
            <textarea id="desc" name="description" class="form-control"></textarea>
        </div>

        <input type="submit" value="Save" class="btn btn-success"/>
    {!! Form::close() !!}<!-- form -->
    </div>
@endsection