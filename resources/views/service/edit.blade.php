@extends("layouts.app")

@section("content")
	<div class="container">
		{!! Form::open(['method' => 'post', 'url' => 'admin/service/update/' .$service->service_id]) !!}
		<h2>Edit service #{!! $service->service_id !!}</h2>
		@if(Session::has('success'))
			<div class="alert alert-success">
				{!! \Illuminate\Support\Facades\Session::get('success') !!}
			</div>
		@endif
		@if(count($errors->all()) > 0)
			<div class="alert alert-danger">
				@foreach ($errors->all() as $message)
					{!! $message !!} <br>
				@endforeach
			</div>
		@endif
		<div class="form-group">
			<label for="name">Service name</label>
			<input id="name" type="text" required class="form-control" value="{!! $service->service_name !!}"
			       name="service_name"/>
		</div>
		
		<div class="form-group">
			<label for="short-desc">Short description</label>
			<input id="short-desc" type="text" class="form-control" value="{!! $service->short_description !!}"
			       name="short_description"/>
		</div>
		
		<div class="form-group">
			<label for="desc">Description</label>
			<textarea id="desc" name="description" class="form-control">{!! $service->description !!}</textarea>
		</div>
		
		<input type="submit" value="Save" class="btn btn-success"/>
	{!! Form::close() !!}<!-- form -->
	</div>
@endsection
